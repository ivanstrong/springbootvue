package org.dev.util.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dev.util.entity.SysLog;

/**
 * <p>
 * 操作记录 服务类
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-06
 */
public interface SysLogService extends IService<SysLog> {

}
