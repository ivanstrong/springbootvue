package org.dev.auth.module.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.dev.auth.module.entity.SysRoleUser;

/**
 * <p>
 * 角色用户关系表 Mapper 接口
 * </p>
 *
 * @author hlt
 * @since 2020-06-24
 */
public interface SysRoleUserMapper extends BaseMapper<SysRoleUser> {

}
