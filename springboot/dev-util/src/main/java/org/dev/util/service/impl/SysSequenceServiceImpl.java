package org.dev.util.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dev.util.entity.SysSequence;
import org.dev.util.mapper.SysSequenceMapper;
import org.dev.util.service.SysSequenceService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * sys_sequence 服务实现类
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-08
 */
@Service
public class SysSequenceServiceImpl extends ServiceImpl<SysSequenceMapper, SysSequence> implements SysSequenceService {

}
