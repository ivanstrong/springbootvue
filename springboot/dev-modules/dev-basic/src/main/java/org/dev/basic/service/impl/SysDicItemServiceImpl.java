package org.dev.basic.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dev.basic.entity.SysDicItem;
import org.dev.basic.mapper.SysDicItemMapper;
import org.dev.basic.service.SysDicItemService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 数据库字典组-子级 服务实现类
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-05
 */
@Service
public class SysDicItemServiceImpl extends ServiceImpl<SysDicItemMapper, SysDicItem> implements SysDicItemService {

}
