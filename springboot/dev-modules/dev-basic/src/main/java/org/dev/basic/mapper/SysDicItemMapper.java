package org.dev.basic.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.dev.basic.entity.SysDicItem;

/**
 * <p>
 * 数据库字典组-子级 Mapper 接口
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-05
 */
public interface SysDicItemMapper extends BaseMapper<SysDicItem> {

}
