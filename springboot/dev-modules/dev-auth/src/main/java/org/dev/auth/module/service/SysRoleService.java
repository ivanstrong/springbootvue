package org.dev.auth.module.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dev.auth.module.entity.SysRole;

/**
 * <p>
 * 权限管理-角色管理 服务类
 * </p>
 *
 * @author hlt
 * @since 2020-06-24
 */
public interface SysRoleService extends IService<SysRole> {

}
