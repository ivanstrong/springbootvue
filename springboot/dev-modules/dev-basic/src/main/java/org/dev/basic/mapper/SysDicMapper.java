package org.dev.basic.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.dev.basic.entity.SysDic;

/**
 * <p>
 * 数据库字典组 Mapper 接口
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-07-05
 */
public interface SysDicMapper extends BaseMapper<SysDic> {

}
