package org.dev.basic.service;


import com.baomidou.mybatisplus.extension.service.IService;
import org.dev.basic.entity.SysJobs;

/**
 * <p>
 * 岗位管理 服务类
 * </p>
 *
 * @author dean.x.liu
 * @since 2020-10-22
 */
public interface SysJobsService extends IService<SysJobs> {

}
