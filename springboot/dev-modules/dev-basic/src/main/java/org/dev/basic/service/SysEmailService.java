package org.dev.basic.service;

import org.dev.basic.entity.SysEmail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author dean.x.liu
 * @since 2022-01-27
 */
public interface SysEmailService extends IService<SysEmail> {

}
